import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:shopp_app/providers/products.dart';
import 'package:shopp_app/widgets/product_item.dart';

// ignore: must_be_immutable
class ProductsGrid extends StatelessWidget {
  bool showFaws;
  ProductsGrid(this.showFaws, {super.key});

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    final products = showFaws ? productsData.favoriteItems : productsData.items;

    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: products.length,
      itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
        value: products[i],
        child: const ProductItem(
            // products[i].id,
            // products[i].title,
            // products[i].imageUrl,
            ),
      ),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }
}
