import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:shopp_app/providers/cart.dart';
import 'package:shopp_app/providers/products.dart';
import 'package:shopp_app/screens/cart_screen.dart';
import 'package:shopp_app/widgets/app_drawer.dart';
import 'package:shopp_app/widgets/badge.dart';
import 'package:shopp_app/widgets/products_grid.dart';

enum FilterOptions { favorites, all }

class ProductOverwievScreen extends StatefulWidget {
  const ProductOverwievScreen({super.key});

  @override
  State<ProductOverwievScreen> createState() => _ProductOverwievScreenState();
}

class _ProductOverwievScreenState extends State<ProductOverwievScreen> {
  var _showFavoritesOnly = false;
  var _isLoading = false;
  var _isInit = false;

  @override
  void initState() {
    // Provider.of<Products>(context, listen: false).fetchAndSetProducts();
    // Future.delayed(Duration.zero)
    //     .then((_) => {Provider.of<Products>(context).fetchAndSetProducts()});
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Products>(context).fetchAndSetProducts().then((_) => {
            setState(() {
              _isLoading = false;
            })
          });
    }
    _isInit = true;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sample Code'),
        actions: [
          PopupMenuButton(
            onSelected: (FilterOptions selectedValue) {
              setState(() {
                if (selectedValue == FilterOptions.favorites) {
                  _showFavoritesOnly = true;
                } else {
                  _showFavoritesOnly = false;
                }
              });
            },
            icon: const Icon(
              Icons.more_vert,
            ),
            itemBuilder: (_) => [
              const PopupMenuItem(
                value: FilterOptions.favorites,
                child: Text('Only favorites'),
              ),
              const PopupMenuItem(
                value: FilterOptions.all,
                child: Text('Show all'),
              ),
            ],
          ),
          Consumer<Cart>(
            child: IconButton(
              icon: const Icon(
                Icons.shopping_cart,
              ),
              onPressed: () {
                Navigator.of(context).pushNamed(CartScreen.routeName);
              },
            ),
            builder: (_, cart, ch) => Badge(
              value: cart.itemCount.toString(),
              color: Colors.red,
              child: ch!,
            ),
          )
        ],
      ),
      drawer: const AppDrawer(),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ProductsGrid(_showFavoritesOnly),
    );
  }
}
