import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shopp_app/models/http_exceptions.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageUrl,
    this.isFavorite = false,
  });

  Future<void> toggleFavoriteStatus(String? authToken, String userId) async {
    final oldStatus = isFavorite;

    isFavorite = !isFavorite;

    final url = Uri.parse(
        'https://shopp-app-26ef0-default-rtdb.europe-west1.firebasedatabase.app/userFavorites/$userId/$id.json?auth=$authToken');

    try {
      final res = await http.put(url, body: json.encode(isFavorite));
      if (res.statusCode >= 400) {
        throw HttpException('Failed to favorite product');
      }
    } catch (e) {
      isFavorite = oldStatus;
      rethrow;
    }
    notifyListeners();
  }
}
